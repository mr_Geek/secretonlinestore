# Secret Shopping Store
===========

> I searched for Symfony2 full prject tutorials in order to get my hands on coding in symfony fast, so i started by this one (REF: https://www.sitepoint.com/symfony2-registration-login/), as i decided to implement the user authentication first.
> From there i knew about Doctrine ORM, Symfony Roles, MVC Structure of Symfony2, Handling Routes, Security layer in symfony, Services for after login processing, Symfony Forms, and Twig.

> One of the big obstacles i faced with implementing Many to Many relationship between Cart and Product entities, as Doctrine auto generates the CartProduct table automatically without any extra customized fields (e.g. amount), so i had to implement the Many to One and One to Many to overcome this problem with my custom table. 
> (REF: https://knpuniversity.com/screencast/symfony2-ep3/many-to-many-relationship)

### What is done:
- Admin invitation is required to register. (REF: https://www.sitepoint.com/symfony2-registration-login/)
- CRUD Operations on product entity that controlled only by admin.
- User Shopping Cart with basic operations (Add, Remove, Empty)
- Used Single Table Inheritance to create Wish List Cart Entity and inherte Cart entity (REF: http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/inheritance-mapping.html#single-table-inheritance)
- Cart entity is the base class for: ShoppingCart entity, and WishCart entity, Thus this fulfills the ability to programitaclly add any type of carts
- Modefied Cart Controller actions to accept ShoppingCart and WishCart types.
- Now the whole task is done.. i think.

### TO BE DONE:
- I think i figured it out.
- UI, and UX sucks.
