<?php

namespace CartBundle\Handler;

use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Http\HttpUtils;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler {

  protected $container;

  public function __construct(HttpUtils $httpUtils, ContainerInterface $cont, array $options){
    parent::__construct($httpUtils, $options);
    $this->container = $cont;
  
  }

  public function onAuthenticationSuccess(Request $req, TokenInterface $token) {
    
    $user = $token->getUser();
    $user->setLogged(new \DateTime());

    $em = $this->container->get('doctrine.orm.entity_manager');

    $em->persist($user);
    $em->flush();

    return $this->httpUtils->createRedirectResponse($req, $this->determineTargetUrl($req));
  }

}