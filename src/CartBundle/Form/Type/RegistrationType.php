<?php

namespace CartBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType {
  
  public function buildForm(FormBuilderInterface $builder, array $options) {

    $builder->add('username', 'text', ['label'=>'Username'])
            ->add('password', 'password', ['label'=>'Password'])
            ->add('confirm', 'password', ['label'=>'Retype Password', 'mapped'=>false])
            ->add('homepage', 'text', ['label'=>'Hompage'])
            ->add('email', 'hidden', ['label'=>'email'])
            ->add('save', 'submit', ['label'=>'Register']);
    
  }

  public function configureOptions(OptionsResolver $resolver){
    $resolver->setDefaults([
      'data-class' => 'CartBundle\Entity\User'
    ]);
  }

}
