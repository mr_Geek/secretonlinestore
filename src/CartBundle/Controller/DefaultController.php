<?php

namespace CartBundle\Controller;

use CartBundle\Entity\Product;
use CartBundle\Entity\NormalProduct;
use CartBundle\Entity\SaleProduct;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
  /**
    * @Route("/")
    */
  public function indexAction()
  {
    $products = $this->getDoctrine()
                      ->getRepository(Product::class)
                      ->findAll();
    if (!$products){
      throw $this->createNotFoundException(
      'No products found'
      );
    }
    return $this->render('CartBundle:Default:index.html.twig', array(
      'products' => $products
    ));
  }

  public function errorAction() {
    return $this->render('CartBundle:Default:error.html.twig');
  }


  public function showProductsAction(){
    $em = $this->getDoctrine()->getManager();
    
    $products = $this->getDoctrine()
                      ->getRepository(Product::class)
                      ->findAll();
                      
    return $this->render('CartBundle:Default:products.html.twig', array(
      'products' => $products
    ));      
  }
  
  public function updateProductPageAction($productId, $productType){
    $em = $this->getDoctrine()->getManager();
    if ($productType == "Normal") {
      $product = $em->getRepository(NormalProduct::class)->find($productId);      
    } elseif ($productType == "Sale") {
      $product = $em->getRepository(SaleProduct::class)->find($productId);            
    } else {
      $this->addFlash('error', 'No product type was specified');
      return $this->redirectToRoute('error');
    }
    if (!$product){
      $this->addFlash('error', 'No product found for id ' . $productId);
      return $this->redirectToRoute('error');
    }
    $em->flush();
    return $this->render('CartBundle:Default:updateProduct.html.twig', array(
      'product' => $product
    ));
  }

  /**
    * Action that updates product based on its type
    */
  public function updateProductAction(Request $req){
    $productId    = $req->get('id');
    $productName  = $req->get('name');
    $productDesc  = $req->get('desc');
    $productPrice = $req->get('price');
    $productType = $req->get('type');

    $em = $this->getDoctrine()->getManager();
    if ($productType == "Normal") {
      $product = $em->getRepository(NormalProduct::class)->find($productId);      
    } elseif ($productType == "Sale") {
      $product = $em->getRepository(SaleProduct::class)->find($productId);            
    } else {
      $this->addFlash('error', 'No product type was specified');
      return $this->redirectToRoute('error');
    }
    if (!$product){
      $this->addFlash('error', 'No product found for id ' . $productId);
      return $this->redirectToRoute('error');
    }
    $product->setName($productName);
    $product->setDescription($productDesc);
    $product->setPrice($productPrice);
    $em->flush();

    $url = $this->generateUrl('showProducts');
    return $this->redirect($url);   
  }

  /**
    * Action that removes product based on its type and id 
    */
  public function removeProductAction($productId, $productType){
    $em = $this->getDoctrine()->getManager();
    if ($productType == "Normal") {
      $product = $em->getRepository(NormalProduct::class)->find($productId);      
    } elseif ($productType == "Sale") {
      $product = $em->getRepository(SaleProduct::class)->find($productId);            
    } else {
      $this->addFlash('error', 'No product type was specified');
      return $this->redirectToRoute('error');
    }
    if (!$product){
      $this->addFlash('error', 'No product found for id ' . $productId);
      return $this->redirectToRoute('error');
    }
    $em->remove($product);
    $em->flush();

    $url = $this->generateUrl('showProducts');
    return $this->redirect($url);   
  }

  public function addProductPageAction(){
    return $this->render('CartBundle:Default:addProduct.html.twig');
  }

  /**
    * Action that adds new product based on its type
    */
  public function addProductAction(Request $req){
    $productName  = $req->get('name');
    $productDesc  = $req->get('desc');
    $productPrice = $req->get('price');
    $productType  = $req->get('type');

    $product = $productType == 'Normal' ? new NormalProduct() : new SaleProduct();
    $product->setName($productName);
    $product->setDescription($productDesc);
    $product->setPrice($productPrice);

    $em = $this->getDoctrine()->getManager();
    $em->persist($product);
    $em->flush();

    // return new Response();
    $url = $this->generateUrl('showProducts');
    return $this->redirect($url); 
  }


}
