<?php

namespace CartBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use CartBundle\Entity\Invite;
use CartBundle\Entity\User;
use CartBundle\Form\Type\RegistrationType;
use \Doctrine\Common\Util\Debug;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{

  
  public function loginAction(Request $req){

    $authenticationUtils = $this->get('security.authentication_utils');

    $error = $authenticationUtils->getLastAuthenticationError();

    $lastUsername = $authenticationUtils->getLastUsername();

    return $this->render('CartBundle:Security:login.html.twig', array(
      'last_username' => $lastUsername,
      'error'         => $error
    ));

  }

  public function inviteAction(){

    return $this->render('CartBundle:Security:invite.html.twig');
  }

  public function doInviteAction(Request $req) {
    $email = $req->get('email');
    $userid = $this->getUser()->getId();

    $hash = $this->setInvite($userid, $email);
    $this->sendEmail($email, $hash);

    $url = $this->generateUrl('invite');

    return $this->redirect($url);
  }

  private function setInvite($userid, $email) {
    $em = $this->getDoctrine()->getManager();

    $user_repo = $em->getRepository('CartBundle:User');
    $user = $user_repo->find($userid);

    $invite = new Invite();
    $invite->setInvited($email);
    $invite->setWhoInvite($user);

    $now = new \DateTime();
    $int = new \DateInterval('P1D'); // Period 1 Day

    $now->add($int);
    $invite->setExpires($now); 

    $random = rand(10000, 99999);

    $invite->setHash($random);

    $em->persist($invite);

    $em->flush();

    return $random;
  }

  private function sendEmail($email, $hash) {
    $mailer = $this->get('mailer');

    $message = $mailer->createMessage()
                      ->setSubject('Someone invites to private online store')
                      ->setFrom('root@ecommerce.com')
                      ->setTo($email)
                      ->setBody(
                        $this->renderView('CartBundle:Default:email.html.twig',['email'=>$email, 'hash'=>$hash]),
                        'text/html'
                      );
    
    $mailer->send($message);

  }

  public function preregisterAction(Request $req){
    $email = $req->get('email');
    $invitationCode = $req->get('code');
    
    try{
      $this->verify($email, $invitationCode);
    } catch(\Exception $e) {
      $this->addFlash('error', $e->getMessage());
      return $this->redirectToRoute('error');
    }

    $registeration = new User();

    $form          = $this->createForm(new RegistrationType(), $registeration, [
                      'action' => $this->generateUrl('createUser'),
                      'method' => 'POST'
                    ]);
    return $this->render('CartBundle:Security:register.html.twig', [
      'form' => $form->createView(), 'email' => $email
    ]);
  }

  private function verify($email, $code){
    $repo = $this->getDoctrine()->getManager()->getRepository('CartBundle:Invite');

    $invites = $repo->findBy(['invited' => $email]);

    if (count($invites) == 0) {
      throw new \Exception("This email is not invited. Email: " . $email);
    }

    dump(count($invites));
    
    dump($invites[ count($invites) - 1 ]);

    $invite = $invites[count($invites) - 1];

    $exp = $invite->getExpires();
    $now = new \DateTime();

    if ($exp < $now) {
      throw new \Exception("Invitation Expired.");
    }

    if ($invite->getHash() !== $code) {
      throw new \Exception("Wrong invitation code, code suppposed to be: (" . $invite->getHash() . "), and you got: (" . $code . ").");
    }

  }

}
