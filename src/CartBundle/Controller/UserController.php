<?php

namespace CartBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use CartBundle\Entity\User;
use CartBundle\Form\Type\RegistrationType;
use \Doctrine\Common\Util\Debug;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
  public function createUserAction(Request $req) {
    $em = $this->getDoctrine()->getManager();
    $form = $this->createForm(new RegistrationType(), new User());
    $form->handleRequest($req);

    $user = new User();
    $user = $form->getData();

    dump($user);
    
    $user->setCreatedAt(new \DateTime());
    $user->setRoles('ROLE_USER');
    $user->setGravatar('http://www.gravatar.com/avatar/'. md5(trim($req->get('email'))) );
    $user->setActive(true);

    $pwd = $user->getPassword();
    $encoder = $this->container->get('security.password_encoder');
    $pwd = $encoder->encodePassword($user, $pwd);
    $user->setPassword($pwd);


    $em->persist($user);

    $em->flush();

    $url = $this->generateUrl('login');
    return $this->redirect($url);
  }
}
