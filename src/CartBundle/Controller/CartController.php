<?php

namespace CartBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use CartBundle\Entity\Product;
use CartBundle\Entity\Cart;
use CartBundle\Entity\WishCart;
use CartBundle\Entity\ShoppingCart;
use CartBundle\Entity\ProductCart;
use \Doctrine\Common\Util\Debug;


class CartController extends Controller
{


  /**
   * Action that handles returning current user's cart
   * @return ProductCart object
   * 
   */
  public function showCartAction($cartType){
    // Get current user's id
    $current_user = $this->getUser()->getId();

    // Get Cart of that user based on the passed type
    if ($cartType == 'Cart'){
      $cart = $this->getDoctrine()
                    ->getRepository(ShoppingCart::class)
                    ->findByUserId($current_user);
    } elseif ($cartType == 'WishCart') {
      $cart = $this->getDoctrine()
                    ->getRepository(WishCart::class)
                    ->findByUserId($current_user);
    } else {
      $this->addFlash('error', 'No cart specified');
      return $this->redirectToRoute('error');
    }

    // dump($cart);
    // dump($cart[0]->getId());
    // dump($cart[0]->getName());
    // dump($cart[0]->getProductCarts());
    

    // If he does not has a cart return flash and redirect to error
    if (!$cart){
      $this->addFlash('error', 'You do not have any items in your cart, add items first before showin your cart.');
      return $this->redirectToRoute('error');
    }

    // Get the products of this cart
    $productCart = $this->getDoctrine()
                        ->getRepository(ProductCart::class)
                        ->findByCart($cart[0]->getId());
    
    // Return them all
    return $this->render('CartBundle:Default:cart.html.twig', array(
      'products' => $productCart,
      'cartType' => $cartType
    ));
    // return new Response('');

  }

  /**
   * Action that handles adding new product to a cart
   * 
   */
  public function addProductAction($productId, $cartType){
    // Initialize Doctrine manager
    $manager = $this->getDoctrine()->getManager();

    // First get current userID
    $current_user = $this->getUser()->getId();

    // Get the product
    $product = $manager->find('CartBundle:Product', $productId);

    // dump($cartType);

    // Get current user's cart based on the passed type
    if ($cartType == 'Cart'){
      $cart = $manager->getRepository(ShoppingCart::class)->findByUserId($current_user);
    } elseif ($cartType == 'WishCart') {
      $cart = $manager->getRepository(WishCart::class)->findByUserId($current_user);
    } else {
      $this->addFlash('error', 'No cart specified');
      return $this->redirectToRoute('error');
    }

    // dump($cart[0]);
    // dump($cart[0] instanceof Cart);
    // dump($cart[0] instanceof WishCart);
    // dump($cart[0] instanceof ShoppingCart);
    
    // If There is no cart
    if (!$cart || count($cart) == 0) {
      // Initialize new cart with user's id Based on cart Type
      $newCart = $cartType == 'Cart'? new ShoppingCart() : new WishCart;
      $newCart->setName('cartUser' . $current_user);
      $newCart->setUserId($current_user);
      $manager->persist($newCart);

      // Create new product and set the amount to 1
      $productCart = new ProductCart();
      $productCart->setAmount(1);
  
      // No need to call $manager->persist($productCart) becuase of cascade={"persist"}
      $product->addProductCart($productCart);
      $newCart->addProductCart($productCart);
  
      $manager->flush();

    } else {
      // Get the product of current cart 
      $productCart = $manager->getRepository(ProductCart::class)->findOneBy(array(
        'cart' => $cart[0]->getId(),
        'product' => $productId
      ));

      // If not intialize new one
      if (!$productCart){
        $productCart = new ProductCart();
        $productCart->setAmount(1);
      }
      
      // Check if product exists in this cart 
      // If it is there increase the amount by 1
      if (in_array($product, $cart[0]->getProducts())){
        // dump($productCart->getAmount());
        $amount = $productCart->getAmount() + 1 ;
        $productCart->setAmount($amount);
      }

      // Finally add the product
      $product->addProductCart($productCart);
      $cart[0]->addProductCart($productCart);
      $manager->flush();
    }
    
    $url = $this->generateUrl('showCart', array(
      'cartType' => $cartType
    ));
    return $this->redirect($url);   
    // return new Response('');
  }

  /**
   * Action that handles removing product from cart
   * 
   */
  public function removeProductAction($productId, $cartType){
    // Initialize Doctrine manager
    $manager = $this->getDoctrine()->getManager();

    // We need product_id, current cart_id to get the current ProductCart of this product
    // Then we find this row in the ProductCart table 
    // Then we delete this row or decrease the amount of this product

    // First get current userID
    $current_user = $this->getUser()->getId();
    
    // Get current user's cart
    if ($cartType == 'Cart'){
      $cart = $manager->getRepository(ShoppingCart::class)->findByUserId($current_user);
    } elseif ($cartType == 'WishCart') {
      $cart = $manager->getRepository(WishCart::class)->findByUserId($current_user);
    } else {
      $this->addFlash('error', 'No cart specified');
      return $this->redirectToRoute('error');
    }

    // Get the cart id
    $cart_id = $cart[0]->getId();

    // find ProductCart row by cart_id and product_id
    $productCart = $manager->getRepository(ProductCart::class)->findOneBy(array(
      'cart' => $cart_id,
      'product' => $productId
    ));

    // Now remove this product from cart
    $cart[0]->removeProductCart($productCart);
    
    $manager->flush();

    $url = $this->generateUrl('showCart', array(
      'cartType' => $cartType
    ));
    return $this->redirect($url); 

  }

  /**
   * Action that handles removing all product quantity from cart
   * 
   */
   public function removeAllTypeAction($productId, $cartType){
    // Initialize Doctrine manager
    $manager = $this->getDoctrine()->getManager();

    // First get current userID
    $current_user = $this->getUser()->getId();

    // Get current user's cart
    if ($cartType == 'Cart'){
      $cart = $manager->getRepository(ShoppingCart::class)->findByUserId($current_user);
    } elseif ($cartType == 'WishCart') {
      $cart = $manager->getRepository(WishCart::class)->findByUserId($current_user);
    } else {
      $this->addFlash('error', 'No cart specified');
      return $this->redirectToRoute('error');
    }

    // Get the cart id
    $cart_id = $cart[0]->getId();

    // find ProductCart row by cart_id and product_id
    $productCart = $manager->getRepository(ProductCart::class)->findOneBy(array(
      'cart' => $cart_id,
      'product' => $productId
    ));

    // Set the amount to 1, to avoid decreasing the amount by removeProductCart()
    $productCart->setAmount(1);

    // Now remove this product from cart
    $cart[0]->removeProductCart($productCart);
    
    $manager->flush();

    $url = $this->generateUrl('showCart', array(
      'cartType' => $cartType
    ));
    return $this->redirect($url); 

  }

  /**
   * Action that handles removing all products from cart
   * and removing cart itslef
   * 
   */
  public function emptyCartAction($cartType){
    // Initialize Doctrine manager
    $manager = $this->getDoctrine()->getManager();

    // First get current userID
    $current_user = $this->getUser()->getId();

    // Get current user's cart
    if ($cartType == 'Cart'){
      $cart = $manager->getRepository(ShoppingCart::class)->findOneByUserId($current_user);
    } elseif ($cartType == 'WishCart') {
      $cart = $manager->getRepository(WishCart::class)->findOneByUserId($current_user);
    } else {
      $this->addFlash('error', 'No cart specified');
      return $this->redirectToRoute('error');
    }

    // find ProductCart row by cart_id
    $productCarts = $manager->getRepository(ProductCart::class)->findBy(array(
      'cart' => $cart->getId()
    ));

    // Loop and remove every product row
    foreach($productCarts as $productCart){
      $manager->remove($productCart);
    }

    // Now remove the cart row from Cart table
    $manager->remove($cart);

    // DO 'EM ALL
    $manager->flush();

    // return new Response('');

    return $this->redirect('/'); 
  }

}
