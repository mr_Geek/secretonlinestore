<?php

namespace CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cart
 *
 * @ORM\Table(name="cart")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"cart" = "Cart", "wishcart" = "WishCart", "shopping" = "ShoppingCart"})
 * @ORM\Entity(repositoryClass="CartBundle\Repository\CartRepository")
 */
class Cart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

     /**
     * @var int
     *
     * @ORM\Column(name="userId", type="integer", nullable=true)
     */
     private $userId;

    /**
     * @ORM\Id()
     * @ORM\OneToMany(targetEntity="CartBundle\Entity\ProductCart", mappedBy="cart", cascade={"persist", "remove"}, orphanRemoval=TRUE)
     */
    private $productCarts;

    
    public function __construct(){
        $this->productCarts = new ArrayCollection();
    }
    

    public function getProductCarts()
    {
        return $this->productCarts->toArray();
    }

    public function addProductCart(ProductCart $productCart){
        if(!$this->productCarts->contains($productCart)){
            $this->productCarts->add($productCart);
            $productCart->setCart($this);
        }
        return $this;
    }
    
    public function removeProductCart(ProductCart $productCart){
        if($this->productCarts->contains($productCart)){
            // Get the amount of this product
            $amount = $productCart->getAmount();
            // This is the last item of this product
            if ($amount == 1){
                // Totaly remove it from the cart
                $this->productCarts->removeElement($productCart);
                $productCart->setCart(null);
            }
            else {
                // Else decrease the amount by 1
                $productCart->setAmount($amount - 1);
            }
        }
        return $this;
    }


    public function getProducts(){
        // Apply function to each element of productCarts array
        return array_map(
            function ($productCart) {
                return $productCart->getProduct();
            },
            $this->productCarts->toArray()
        );
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Cart
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Cart
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Cart
     */
     public function setUserId($userId)
     {
         $this->userId = $userId;
 
         return $this;
     }
 
     /**
      * Get userId
      *
      * @return int
      */
     public function getUserId()
     {
         return $this->userId;
     }
}
