<?php

namespace CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"normal" = "NormalProduct", "sale" = "SaleProduct", "product" = "Product"})
 * @ORM\Entity(repositoryClass="CartBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Id()
     * @ORM\OneToMany(targetEntity="CartBundle\Entity\ProductCart", mappedBy="product", cascade={"persist", "remove"}, orphanRemoval=TRUE)
     */
    private $productCarts;


    public function __construct(){
        $this->productCarts = new ArrayCollection();
    }

    public function getProductCarts()
    {
        return $this->productCarts->toArray();
    }

    public function addProductCart(ProductCart $productCart){
        if(!$this->productCarts->contains($productCart)){
            $this->productCarts->add($productCart);
            $productCart->setProduct($this);
        }
        return $this;
    }
    
    public function removeProductCart(ProductCart $productCart){
        if($this->productCarts->contains($productCart)){
            $amount = $productCart->getAmount();
            if ($amount == 1){
                $this->productCarts->removeElement($productCart);
                $productCart->setProduct(null);
            }
            else {
                $productCart->setAmount($amount - 1);
            }
        }
        return $this;
    }

    public function getCarts(){
        return array_map(
            function ($productCart) {
                return $productCart->getCart();
            },
            $this->productCarts->toArray()
        );
    }


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
