<?php

namespace CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CartBundle\Entity\Product;


/**
 * NormalProduct
 *
 * @ORM\Table(name="normal_product")
 * @ORM\Entity(repositoryClass="CartBundle\Repository\NormalProductRepository")
 */
class NormalProduct extends Product
{
    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type = 'Normal';

    public function getType(){
        return $this->type;
    }

}

