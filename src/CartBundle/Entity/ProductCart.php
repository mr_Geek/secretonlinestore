<?php

namespace CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CartBundle\Entity\Product;
use CartBundle\Entity\Cart;

/**
 * ProductCart
 *
 * @ORM\Table(name="product_cart")
 * @ORM\Entity(repositoryClass="CartBundle\Repository\ProductCartRepository")
 */
class ProductCart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="productCarts")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=FALSE)
     * */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="productCarts")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id", nullable=FALSE))
     * */
    protected $cart;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    public function getAmount(){
        return $this->amount;
    }

    public function setAmount($amount){
        $this->amount = $amount;
        return $this;
    }




    public function getCart(){
        return $this->cart;
    }

    public function setCart(Cart $cart = null){
        $this->cart = $cart;
        return $this;
    }






    public function getProduct(){
        return $this->product;
    }

    public function setProduct(Product $product = null){
        $this->product = $product;
        return $this;
    }


    
}

