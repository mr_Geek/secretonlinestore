<?php

namespace CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invite
 *
 * @ORM\Table(name="invite")
 * @ORM\Entity(repositoryClass="CartBundle\Repository\InviteRepository")
 */
class Invite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="invited", type="string", length=255, nullable=true)
     */
    private $invited;

    /**
     * @var string
     *
     * @ORM\Column(name="whoInvite", type="object", nullable=true)
     */
    private $whoInvite;

    /**
     * @var string
     *
     * @ORM\Column(name="expires", type="datetime", nullable=true)
     */
    private $expires;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=true)
     */
    private $hash;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invited
     *
     * @param string $invited
     * @return Invite
     */
    public function setInvited($invited)
    {
        $this->invited = $invited;

        return $this;
    }

    /**
     * Get invited
     *
     * @return string 
     */
    public function getInvited()
    {
        return $this->invited;
    }

    /**
     * Set whoInvite
     *
     * @param string $whoInvite
     * @return Invite
     */
    public function setWhoInvite($whoInvite)
    {
        $this->whoInvite = $whoInvite;

        return $this;
    }

    /**
     * Get whoInvite
     *
     * @return string 
     */
    public function getWhoInvite()
    {
        return $this->whoInvite;
    }

    /**
     * Set expires
     *
     * @param string $expires
     * @return Invite
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * Get expires
     *
     * @return string 
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Invite
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }
}
