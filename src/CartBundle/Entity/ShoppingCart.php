<?php

namespace CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CartBundle\Entity\Cart;

/**
 * WishCart
 *
 * @ORM\Table(name="wish_cart")
 * @ORM\Entity(repositoryClass="CartBundle\Repository\WishCartRepository")
 */
class ShoppingCart extends Cart
{
    
}

