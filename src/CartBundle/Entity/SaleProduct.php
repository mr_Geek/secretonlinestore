<?php

namespace CartBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CartBundle\Entity\Product;


/**
 * SaleProduct
 *
 * @ORM\Table(name="sale_product")
 * @ORM\Entity(repositoryClass="CartBundle\Repository\SaleProductRepository")
 */
class SaleProduct extends Product
{
    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type = 'Sale';

    public function getType(){
        return $this->type;
    }
}

